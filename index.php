<!DOCTYPE html>
<html>
    <head>
        <title>Svatikk Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                
        <meta>
        <link rel="shortcut icon" type="image/png" href="images/favicon-32x32.png"/>
        <link rel="stylesheet" href="style/wave.css">  
        <link rel="stylesheet" href="style/play_ground.css">  
        <link rel="stylesheet" href="style/main.css">  
        <link href="https://fonts.googleapis.com/css?family=Raleway:200,100,400" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script>
            function myFunction(x){x.classList.toggle("change"); }
        </script>                        
        <!-- Here we create a query for slide show-->
        <style>  
            * {margin:0px;padding:0px;}
            *, *:after, *:before { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box; box-sizing: border-box; }

            @media (max-width:727px){
                .header_menu{display: none;}                                
                .bars101{ display: inline-block; cursor: pointer; margin: 0 15px 20px; }
                .bars1,.bars2{ width: 25px; height: 2px; background-color: #ffffff; margin: 6px 0; transition: 0.4s;}                
                .change .bars1{-webkit-transform: rotate(-45deg) translate(5px , 0px); transform: rotate(-45deg) translate(5px , 0px);}
                .change .bars2{-webkit-transform: rotate(45deg) translate(-5px , -12px); transform: rotate(45deg) translate(-5px , -12px);}                                                                
                .container{height: 3200px;}
                .footer{height: 121px; }
                .footer_menu{margin: 0px;text-align: center; float:none;}
            }                        
                        
            @media (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }                        
                        
                        
                        
        </style>
                        
    </head>
    <body>


        
        
<?php include 'header.php';?>

        <div class="slide_show">
            
            <div id="wrapper">
                <div id="title">
                    <h1>Do what you love</h1>
                </div> 
                <canvas id="canvas" width="1950px" height="800px"></canvas>
                <canvas id="canvasbg" width="1950px" height="800px"></canvas>
            </div>
            <script src="js/play_ground.js"></script>
            
        </div> 
        
        


        <div class="section">
                       
            <h3 class="title">Android application</h3>
            <div id="container">
                <div id="card-container">
                    <div class="col-wrapper">
                        <div class="col col-2">
                            <div class="item">
                                <div class="item-img">
                                    <img src="https://lh3.googleusercontent.com/Td6ju8lqdIIIXg9qHBwwA2jNKFkFgNUU5PffiwKHELm8q2FUkWtCUJrjlxRD2N4_boQ=s180">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">The Om app</p>
                                    <p>Om application for spiritual and devotional people</p>
                                    <a href="https://play.google.com/store/apps/details?id=com.svatikk.theomapp&hl=en">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div style="width:100%; height: 200px; position: relative">
                        <div class="waveWrapper waveAnimation">
                            <div class="waveWrapperInner bgMiddle">
                                <div class="wave waveMiddle" style="background-image: url('http://front-end-noobs.com/jecko/img/wave-mid.png')"></div>
                                    
                            </div>
                            <div class="waveWrapperInner bgBottom">
                                <div class="wave waveBottom" style="background-image: url('http://front-end-noobs.com/jecko/img/wave-bot.png')"></div>
                                    
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
                
                  
            </div>
            
  
    
    





        <div class="section2">
            
            <h3 class="title">Libraries</h3>
            <div id="container">
                <div id="card-container">
                    
                    
                    
                    <div class="col-wrapper">
                        <div class="col col-1">
                            <div class="item">
                                <div class="item-img">
                                    <img src="https://raw.githubusercontent.com/Svastikkka/Antima-Style/master/Images/antima-style.png">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">Antima-Style</p>
                                    <p>Powerful front-end library for faster and easier web development.</p>
                                    <a href="https://github.com/Svastikkka/Antima-Style">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-wrapper">
                        <div class="col col-2">
                            <div class="item">
                                <div class="item-img">
                                    <img src="images/Guptalekhanapaddhati.jpeg">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">Guptalekhanapaddhati</p>
                                    <p>Guptalekhanapaddhati , A simple library for enhance security </p>
                                    <a href="https://github.com/Svastikkka/Guptalekhanapaddhati">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>        
        <h3 class="title">Other Work Samples</h3>
            <div id="container">
                <div id="card-container">
                    
                    
                    
                    <div class="col-wrapper">
                        <div class="col col-1">
                            <div class="item">
                                <div class="item-img">
                                    <img src="images/github.png">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">Github</p>
                                    <p>Other Projects </p>
                                    <a href="https://github.com/Svastikkka">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-wrapper">
                        <div class="col col-1">
                            <div class="item">
                                <div class="item-img">
                                    <img src="images/project.jpeg">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">Projects</p>
                                    <p>Projects created by svatikk</p>
                                    <a href="projects.php">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                
            </div>        



<?php include 'footer.php';?>




        
<!-- Simple typing carousel-->
<script>
 var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  var elements = document.getElementsByClassName('txt-rotate');
  for (var i=0; i<elements.length; i++) {
    var toRotate = elements[i].getAttribute('data-rotate');
    var period = elements[i].getAttribute('data-period');
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
  document.body.appendChild(css);
};   
    
    
</script>
    </body>
</html>
