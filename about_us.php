<!DOCTYPE html>
<html>
     <head>
        <title>About us</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="images/favicon-32x32.png"/>
        <link rel="stylesheet" href="style/main.css">  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script>
          function myFunction(x){x.classList.toggle("change"); }
        </script>
        <style>  
            * {margin:0px;padding:0px;}
            *, *:after, *:before { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box; box-sizing: border-box; }
            @media (max-width:727px){
                .header_menu{display: none;}                                
                .bars101{ display: inline-block; cursor: pointer; margin: 0 15px 20px; }
                .bars1,.bars2{ width: 25px; height: 2px; background-color: #ffffff; margin: 6px 0; transition: 0.4s;}                
                .change .bars1{-webkit-transform: rotate(-45deg) translate(5px , 0px); transform: rotate(-45deg) translate(5px , 0px);}
                .change .bars2{-webkit-transform: rotate(45deg) translate(-5px , -12px); transform: rotate(45deg) translate(-5px , -12px);}                                                                
                .container{height: 3200px;}
                .footer{height: 121px; }
                .footer_menu{margin: 0px;text-align: center; float:none;}
            }                        
            @media (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}  
            }                                
        </style>
     </head>
    <body>        
<?php include 'header.php';?>
  
        <div  class="about_us_conatiner">
            <button class="accordion">Who we are ?</button>
                <div class="panel">
                    <p>We service base software development team .</p>
                </div>
            <button class="accordion">Who created this website ?</button>        
            <div class="panel">
                <p>This web site  is created by Manshu Sharma</p>        
            </div>
            <button class="accordion">Where we are from ?</button>
            <div class="panel">
                <p>Svatikk's team is currently presence in India , State:- Rajasthan ,City:- Jodhpur .</p>
            </div>
            <button class="accordion">History</button>
            <div class="panel">
                <p>Svatikk, register by Manshu Sharma, has provided outsourced development services since 2016. We employ a skilled team of developers , who are totally focused on delivering high quality software solutions which enable our customers to achieve their critical IT objectives. </p>
            </div>
            <button class="accordion">Mission</button>
            <div class="panel">
                <p>Our mission is to help enterprises accelerate adoption of new technologies, untangle complex issues that always emerge during digital evolution, and  arrange ongoing innovation.</p>
            </div>
            <center style='padding:5px; margin-bottom: 300px; width: 100%;'><p style="width: 100%;">Thanks to all people who believe in us and help us to making svatikk.com</p></center>
        </div>
        <?php include 'footer.php';?>

        <script>
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
              acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                  panel.style.maxHeight = null;
                } else {
                  panel.style.maxHeight = panel.scrollHeight + "px";
                } 
              });
            }
        </script>            

    </body>
</html>
