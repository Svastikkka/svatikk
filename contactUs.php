<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="style/main.css">  
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="shortcut icon" type="image/png" href="images/favicon-32x32.png"/>


         <script>
           function myFunction(x){x.classList.toggle("change"); }
         </script>       
         <style>
            * {margin:0px;padding:0px;}
            *, *:after, *:before { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box; box-sizing: border-box; }
            .clearfix:before, .clearfix:after { display: table; content: ''; }
            .clearfix:after { clear: both; }
            input:focus, textarea:focus, keygen:focus, select:focus {outline: none;}
            ::-moz-placeholder {color: #666;font-weight: 300;opacity: 1;}
            ::-webkit-input-placeholder {color: #666;font-weight: 300;}

            /* Contact Form Styling */
            .textcenter {text-align: center;margin-top: 100px;}
            .contact-section1 {text-align: center;display: table;width: 100%;}
            .contact-section1 .shtext {	display: block;margin-top: 20px;}
            .contact-section1 .seperator {border-bottom:1px solid #a2a2a2;width: 35px;display: inline-block;margin: 20px;}
            .contact-section1 h1 {font-size: 40px;color: #A44DD8;font-weight: normal;}
            .contact-section2 {display: block;width: 100%; padding: 50px; height: 650px;}




            * {margin:0px;padding:0px;}
            *, *:after, *:before { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box; box-sizing: border-box; }

            @media (max-width:727px){
                                
                .header_menu{display: none;}                                
                .bars101{ display: inline-block; cursor: pointer; margin: 0 15px 20px; }
                .bars1,.bars2{ width: 25px; height: 2px; background-color: #ffffff; margin: 6px 0; transition: 0.4s;}                
                .change .bars1{-webkit-transform: rotate(-45deg) translate(5px , 0px); transform: rotate(-45deg) translate(5px , 0px);}
                .change .bars2{-webkit-transform: rotate(45deg) translate(-5px , -12px); transform: rotate(45deg) translate(-5px , -12px);}                                                                
               
                
                
  
                
                
                .container{height: auto;}
                .footer{height: 121px; }
                .footer_menu{margin: 0px;text-align: center; float:none;}
         
            }                        
                        
            @media (max-height: 450px) {
                            

                
                
                
                
                
                            .sidenav {padding-top: 15px;}
                            .sidenav a {font-size: 18px;}
                        
            }                        
               
            @media only screen and (max-width: 420px) {
                .section1 h1 {font-size: 28px;}	
            }
        </style>
    </head>
    <body>
        
<?php include 'header.php';?>
      
	<div class="container">
		<div class="innerwrap">
			<section class="contact-section1 clearfix">
				<div class="textcenter">
					<h1>Contact us</h1>
					<span class="seperator"></span>
				</div>
			</section>
		
			<section class="contact-section2 clearfix">

				<div class="col2 column2 last">
					<div class="sec2innercont">
						<div class="sec2addr">                                                
                                                        <div class="row form-padding">
                                                            <div class="col-lg-8 col-lg-offset-2">
                                                                <form name="sentMessage" id="contactForm" novalidate="">
                                                                    <div class="row control-group">
                                                                        <div class="form-group col-xs-12 floating-label-form-group controls">
                                                                            <label>Name</label>
                                                                            <input type="text" class="form-control" placeholder="Name" id="name" required="" data-validation-required-message="Please enter your name.">
                                                                            <p class="help-block text-danger"></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row control-group">
                                                                        <div class="form-group col-xs-12 floating-label-form-group controls floating-label-form-group-with-focus">
                                                                            <label>Email Address</label>
                                                                            <input type="email" class="form-control" placeholder="Email Address" id="email" required="" data-validation-required-message="Please enter your email address.">
                                                                            <p class="help-block text-danger"></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row control-group">
                                                                        <div class="form-group col-xs-12 floating-label-form-group controls">
                                                                            <label>Message</label>
                                                                            <textarea rows="5" class="form-control" placeholder="Message" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                                                                            <p class="help-block text-danger"></p>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="contact-btn text-center btn-style">
                                                                        <div class="btn-styleLine double-line page-scroll">
                                                                            <a href="#categories" class="btn btn-lg btn-outline">
                                                                                <i class="fa fa-fw fa-paper-plane"></i>Send
                                                                            </a>
                                                                        </div>
                                                                    </div>    
                                                                </form>
                                                            </div>

                                                        </div>                                                
                                                </div>
					</div>
				</div>
			</section>
            <?php include 'footer.php';?>

		</div>
	</div>





    </body>
</html>
