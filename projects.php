<!DOCTYPE html>
<html>
    <head>
        <title>Svatikk Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                
        <meta>
        <link rel="shortcut icon" type="image/png" href="images/favicon-32x32.png"/>

        <link rel="stylesheet" href="style/wave.css">  
        <link rel="stylesheet" href="style/play_ground.css">  
        <link rel="stylesheet" href="style/main.css">  
        <link href="https://fonts.googleapis.com/css?family=Raleway:200,100,400" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script data-ad-client="ca-pub-6076854858928051" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            function myFunction(x){x.classList.toggle("change"); }
        </script>                        
        <!-- Here we create a query for slide show-->
        <style>  
            * {margin:0px;padding:0px;}
            *, *:after, *:before { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box; box-sizing: border-box; }

            @media (max-width:727px){
                .header_menu{display: none;}                                
                .bars101{ display: inline-block; cursor: pointer; margin: 0 15px 20px; }
                .bars1,.bars2{ width: 25px; height: 2px; background-color: #ffffff; margin: 6px 0; transition: 0.4s;}                
                .change .bars1{-webkit-transform: rotate(-45deg) translate(5px , 0px); transform: rotate(-45deg) translate(5px , 0px);}
                .change .bars2{-webkit-transform: rotate(45deg) translate(-5px , -12px); transform: rotate(45deg) translate(-5px , -12px);}                                                                
                .container{height: 3200px;}
                .footer{height: 121px; }
                .footer_menu{margin: 0px;text-align: center; float:none;}
            }                        
                        
            @media (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }                        
                        
                        
                        
        </style>
                        
    </head>
    <body>
<?php include 'header.php';?>
   
      <div class="section">
                       
            <h3 class="title">Projects</h3>
            <div id="container">
                <div id="card-container">
                    <div class="col-wrapper">
                        <div class="col col-2">
                            <div class="item">
                                <div class="item-img">
                                    <img src="https://lh3.googleusercontent.com/Td6ju8lqdIIIXg9qHBwwA2jNKFkFgNUU5PffiwKHELm8q2FUkWtCUJrjlxRD2N4_boQ=s180">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">The Om app</p>
                                    <p>Om application for spiritual and devotional people</p>
                                    <a href="https://play.google.com/store/apps/details?id=com.svatikk.theomapp&hl=en">See More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-wrapper">
                        <div class="col col-2">
                            <div class="item">
                                <div class="item-img">
                                    <img src="images/InternetSpeedTester.png">
                                </div>
                                <div class="item-desc">
                                    <p class="item-heading">Internet Speed Tester</p>
                                    <p>Test your Internet Speed</p>
                                    <a href="SpeedCheck/speedcheck.php">See More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
                
                  
            
      </div>        

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>      
        
    </body>
</html>

